<?php

namespace App\Http\Controllers;

use App\Pin;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PinController extends Controller
{
    public function index()
    {
        $remaining = Pin::where("valid", 1)->whereNull("taken")->count();

        return view("welcome", compact(["remaining"]));
    }

    public function generate()
    {
        $pins = Pin::where("valid", 1)->whereNull("taken");

        $pin = $pins->inRandomOrder()->first();

        if (!$pin) {
            abort(403, "Out of PIN numbers");
        }

        // we found a PIN
        $pin->taken = Carbon::now();
        $pin->save();

        $remaining = $pins->count();

        return response()->json(compact(["pin", "remaining"]));
    }

    public function reset()
    {
        Pin::query()->update([
            "taken" => null
        ]);

        $remaining = Pin::where("valid", 1)->whereNull("taken")->count();

        return response()->json(compact(["remaining"]));
    }
}

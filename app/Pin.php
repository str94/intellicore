<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pin extends Model
{
    const PIN_LENGTH = 4; // max length of the PIN
    const MIN_DIGIT_USAGE = 3; // the minimum number of different digits that must be used
    const MAX_DIGIT_USAGE = 2; // the maximum number of times a single digit can be used

    protected $guarded = ["id"];

    public $timestamps = false;

    public static function generateHighest()
    {
        $pin = [];

        while (count($pin) < self::PIN_LENGTH) {
            $pin[] = 9;
        }

        return implode("", $pin);
    }

    public static function checkValid($number)
    {
        if (!self::checkLength($number)) return false;
        if (!self::checkMatching($number)) return false;
        if (!self::checkDigitUsage($number)) return false;
        if (!self::checkConsecutive($number)) return false;

        return true;
    }

    public static function checkLength($number)
    {
        // PIN must be 4 digits long
        return strlen($number) === self::PIN_LENGTH;
    }

    public static function checkMatching($number)
    {
        // start at position 1 as position 0 does not have a previous number to test against
        for ($i = 1; $i < strlen($number); $i++) {
            // check if the previous number matches the current number
            if ($number[$i] === $number[$i-1]) {
                // dont allow consecutive matching numbers
                return false;
            }
        }

        return true;
    }

    public static function checkDigitUsage($number)
    {
        // check we are using the required minimum number of distinct digits
        if (count(array_unique(str_split($number))) < self::MIN_DIGIT_USAGE) {
            return false;
        }

        for ($i = 0; $i < strlen($number); $i++) {
            // check we dont go over the max number of times a single digit can be used
            if (substr_count($number, strval($number[$i])) > self::MAX_DIGIT_USAGE) {
                return false;
            }
        }

        return true;
    }

    public static function checkConsecutive($number)
    {
        // check if numbers are running up or down the way
        $order = (intval($number[1]) - intval($number[0])) < 0 ? "DESC" : "ASC";

        for ($i = 1; $i < strlen($number); $i++) {
            if ($order === "ASC") {
                if (intval($number[$i]) !== intval($number[$i-1])+1) {
                    // if the current number is not 1 higher than the previous, pass the test
                    return true;
                }
            } else {
                if (intval($number[$i]) !== intval($number[$i-1])-1) {
                    // if the current number is not 1 lower than the previous, pass the test
                    return true;
                }
            }
        }

        return false;
    }
}

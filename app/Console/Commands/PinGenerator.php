<?php

namespace App\Console\Commands;

use App\Pin;
use Illuminate\Console\Command;

class PinGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pins:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates all possible PIN numbers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        for ($i = 0; $i <= Pin::generateHighest(); $i++) {
            $number = str_pad(strval($i), Pin::PIN_LENGTH, "0", STR_PAD_LEFT);

            $this->line("Testing: $number");

            if ($valid = Pin::checkValid($number)) {
                $this->info("Passed Validation");
            } else {
                $this->warn("Failed Validation");
            }

            // store the PIN in the database
            Pin::create([
                "pin" => $number,
                "valid" => $valid
            ]);
        }
    }
}
